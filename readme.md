This is Windows Hosted(MinGW-w64) GCC version 11.0.1 20210407 cross compiler of arm-linux-androideabi
I did some tweaks to GCC to make it work better on android.
Remove emergency heap in libsupc++. operator new will fail-fast instead of throw global new and global new is now noexcept. Remove libstdc++ verbose terminate handler. std::terminate now calls __builtin_trap() New handler etc., will become no-op.
You can still throw exceptions, and the EH runtime bloat now reduces to 20kbs. That allows us to static linking libstdc++ freely.
Although iostream is still usable, I recommend not to use it since iostream bloats binary for nearly 1 MB.
This GCC also contains my fast_io library (https://github.com/expnkx/fast_io) to replace iostream without C++ runtime bloat and works perfectly with bionic.
